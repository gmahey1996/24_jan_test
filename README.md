<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>test</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../fontawesome/css/all.min.css">
    <script src="../jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <style>
    .img{
        background-image: url("../Images/witcher.jpg");
        background-size: 100% 100%;
        width: 100%;
        height: 754px;
    }
    .img2{
        background-image: url("../Images/3.jpeg");
        background-size: 100% 100%;
        height: 650px;
        width: 600px;
    }
    .img3{
        background-image: url("../Images/background.jpeg");
        background-size: 100% 100%;
        height: 500px;
        width: 900px;
       
        
    }
    .img4{
        background-image: url("../Images/background.jpeg");
        background-size: 100% 100%;
        height: 500px;
        width: 300px;
        float: left;
    }

    .startup h1{
        color: rgb(64, 49, 194);
    }
    .btn1{
        color: whitesmoke;
        background-color: rgb(96, 15, 228);
        border-radius:75px;

    }
    .btn1:hover{
        color: rgb(96, 15, 228);
        border: 1px solid rgb(96, 15, 228) ;
        background-color: transparent;

    }
    .btn2{
        color: whitesmoke;
        background-color: rgb(15, 65, 228);
        border-radius:75px;

    }
    .btn2:hover{
        color: rgb(15, 228, 210);
        border: 1px solid rgb(15, 228, 210) ;
        background-color: transparent;

    }
    .btn4{
        
    }
    .services h4{
        color: blue;
    }
    .card{
        width: 100%;
    }

    .card1,.card2,.card3{
        width: 25%;
        height: 200px;
       display: inline;
        border: 2px solid violet;
    }
    .pera{
        word-spacing: 20px;
        line-height: 30px;
    }

    </style>

</head>
<body>

  <div class="container-fluid">

    <div class="row">
        <div class="col-md-12 px-0 img">
            <nav class="navbar navbar-expand-sm p-2">
            <a href="" class="navbar-brand">StartUp</a>
            <button class="navbar-toggler text-dark bg-success"  data-toggle="collapse" data-target="#menu">
                <span class="navbar-toggler-icon "></span>
            </button>

            <div id="menu" class="collapse navbar-collapse">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a href="" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">Services</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">Portfolio</a>
                </li>
                <li class="nav-item dropdown">
                    <a href=""class="nav-link dropdown-toggle" data-toggle="dropdown">Pages</a>
                    <div class="dropdown-menu"> 
                        <a href="" class="dropdown-item">Portfolio Details</a>
                        <a href="" class="dropdown-item">About</a>
                        <a href="" class="dropdown-item">Elements</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="" class="nav-link dropdown-toggle" data-toggle="dropdown">Blog</a>
                    <div class="dropdown-menu"> 
                        <a href="" class="dropdown-item">Blog</a>
                        <a href="" class="dropdown-item">Single Blog</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">Contact</a>
                </li>
            </ul>
            </div>
            </nav>
            <div class=" startup pl-5 pt-5 mt-5 ml-5">
                <h1 class="text">Startup you can build a <br> website online using the  <br> Bootstrap builder.</h1> <br>
                <button class="btn btn1">Visit Our Works</button>
            </div>

        </div>
    </div>


    <div class="row">
        <div  class="col-md-12 mt-5 services">
            <h4 class="text-center mt-3" >Services</h4>
            <h1 class="text-center"> <strong> With more than 20 years of <br> experience we can deliver the <br> best product design.</strong></h1>
        </div>
    </div>    
    
    <div class="row ">    
        <div class="col-md-12 card">
            <div   class="col-md-4 card1">
                <h2 class="text-center">Graphic Design</h2>
                <p class="text-center">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci natus blanditiis non libero eos quae, ad reprehenderit nobis, omnis ab consequuntur.</p>
            </div  >

            <div   class="col-md-4 card2">
                <h2>Web Design</h2>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci natus blanditiis non libero eos quae, ad reprehenderit nobis, omnis ab consequuntur.</p>
            </div  >
            <div   class="col-md-4 card3">
                <h2>Mobile App</h2>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci natus blanditiis non libero eos quae, ad reprehenderit nobis, omnis ab consequuntur.</p>
            </div  >

        </div>
    </div>


    <div class="row">
        <div class="col-md-6 d-none d-md-block img2">

        </div>
        <div class="col-md-6 bg-dark">
            <h6 class="mt-5 text-light">About Us</h6>
            <h2 class="mt-5 text-white">Empowering individuals</h1>
            <p class="mt-5 text-white pera">Efficiently unleash cross-media tour function information <br> without cross action media value. Quickly maximize timely <br> deliverables for real-time schemas.</p>
            <p class="text-white pera">“Function information without cross action media  <br> value.</p>
            <button class="btn btn2 mb-5">About US</button>
        </div>
        
    
    </div>


    <div class="row">
        <div class="col-md-12">
            <h1 class="text-center mt-5">Featured Works</h1>
        </div>
        <div class="col-md-12">
            <h6 class="text-center">Tour function information without cross action media value quickly <br> maximize timely deliverables.</h6>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 img3"></div>
            <div class="col-md-6 img4"></div>
        </div>    
    </div>

   <div class="row">
       <div class="col-md-6 p-5 ml-5 mt-5">
           <h1> How We Work</h1>
           <p class="text-dark">“Function information without cross action <br> media value.</p>
           <h5>Efficiently unleash cross-media tour function information without cross action media value. <br> Quickly maximize timely deliverables for real-time schemas.</h5>
            <button class="btn btn-4 mb-5"><span><i class="fab fa-youtube"></i></span></button>
            <h6>Watch Video</h6>
       </div>
   </div> 






 </div>   
</body>
</html>